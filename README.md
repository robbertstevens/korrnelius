# [Korrnelius ](http://twitter.com/korrnelius)#
The Twitter bot for the course Advanced Programming made by Jorrit Bakker and Robbert Stevens
![Example](docs/example.png)

# Requirements #
Python 3

# Installation #
1. pip install -r requirements.txt
2. create config.py in /src containing:
consumer_key = YOUR_CONSUMER_KEY
consumer_secret = YOUR_CONSUMER_SECRET
access_token = YOUR_ACCESS_TOKEN
access_token_secret = YOUR_ACCESS_TOKEN_SECRET

# Instructions #
* `--gui` Start the GUI
* `--console` Start the console
    * `--rate <int>` Start the auto post console <int> are in seconds
* `--query <string>` The trending topic Korrnelius has to search for

To start Korrnelius use the parameter `--gui` or `--console` for the GUI version or the console version respectively.
Both the console and the GUI version can make use of the `--query` parameter, but only the console version can use the `--rate` parameter.

example start-ups

* `korrnelius.py --gui --query #aardappel` Starts the GUI and searches for tweets with the query \#aardappel
* `korrnelius.py --console` Start the console version with everything default
* `korrnelius.py --console --rate 3600` Starts the console which will tweet every hour