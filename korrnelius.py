import sys

sys.path.append('src/')

from controller.korrnelius_controller import *
from view.korrnelius_view import *

from twitter import *
from random import randrange


def main(argv):
    """
    The main function
    """
    trend = None
    if "--query" in argv:
        try:
            trend = argv[argv.index("--query") + 1]
            if "--" in trend:
                raise Exception()
        except Exception as e:
            print("--query was entered wrong, usage: --trend <string>",
                  file=sys.stderr)
            exit(-1)

    if "--gui" in argv:
        Korrnelius(trend, GUIView()).run()
    elif "--console" in argv:
        if "--rate" in argv:
            try:
                rate = int(argv[argv.index("--rate") + 1])
            except Exception as e:
                print("--rate was entered wrong, usage: --rate <int>",
                      file=sys.stderr)
                exit(-1)

            Korrnelius(trend, AutoConsoleView(rate)).run()

        Korrnelius(trend, ConsoleView()).run()
    else:
        print("Korrnelius doesn't know what to do enter a parameter",
              file=sys.stderr)
        exit(-1)
if __name__ == "__main__":
    main(sys.argv)
