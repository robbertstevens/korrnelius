import sys
sys.path.append('src/')
from src.rhyme import *
from src.misc import *


def main(argv):

    if len(argv) == 2:
        wd = word_dictionary()
        rd = rhyme_dictionary(wd)

        rhyming = find_rhyming_words(argv[1], wd, rd)
        for r in rhyming[0:5]:
            print(r)

if __name__ == "__main__":
    main(sys.argv)
