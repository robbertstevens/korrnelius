import sys
from random import randrange
from twitter import *
from model.korrnelius_model import Tweet
from rhyme import *
from PIL import Image, ImageDraw, ImageFont
from misc import *
from collections import defaultdict, Counter


class Korrnelius:
    """
    This is the controller for Korrnelius the rhymer on a timer
    follow him on twitter @Korrnelius
    """
    def __init__(self, trend, view):
        self.poem = None
        self.message = ""
        self.view = view
        self.tweets = []
        self.trend = trend

        self.force_random_trend = False

        if trend is None:
            self.force_random_trend = True

        self.schemes = [
            "ABAB",
            "ABBA",
            "AAAA",
        ]
        self.tweet_messages = [
            "Het werk is verricht, een nieuw gedicht: ",
            "Het is geen geheim, een nieuwe rijm: ",
            "Ik ben een poëet, mijn werk is gereed: ",
            "Ons werk is weer sterk:",
            "Voldaan:"
        ]

    def run(self):
        """
        Run the application
        """
        self.view.show()
        # Get dictionary
        self.view.display("Reading the dictionary")
        self.dictionary = word_dictionary()
        self.view.display("Learning all rhyme words")
        self.rhyme_dictionary = rhyme_dictionary(self.dictionary)
        self.view.display("Ready to rhyme")

        while True:
            self.view.display(self.message)

            action = self.view.get_action()

            if action == "1" or action == "new":
                self.view.display("What to write, what to write")
                self.get_tweets()

                if len(self.tweets) > 0:
                    self.poem = self.make_poem()

                    if self.poem is not None:
                        self.message = self.poem[0]

                else:
                    self.view.display("No inspiration! I need more tweets")
            elif action == "2" or action == 'tweet':
                if self.poem is None:
                    self.view.display("Make a poem first")
                else:
                    am = TweepyAPIManager()
                    image = self.make_image(self.poem[0])
                    mentions = '@'+' @'.join(self.poem[1]).strip()
                    tweet = ""
                    while len(tweet) == 0 or len(tweet) > 140:
                        tweet = "{} {} #{} #{}".format(
                            self.tweet_messages[
                                randrange(0, len(self.tweet_messages))
                            ],
                            mentions,
                            self.trend.replace("#", ""),
                            "gedicht"
                        )

                    am.post_tweet_with_media(tweet, open("./tweet.png"))
                    self.view.display("Tweet tweet: we will tweet the poem")
            elif action == "3" or action == 'quit':
                self.view.display("Bye bye, see you next time")
                break
            else:
                raise Exception('Invalid action: {}'.format(action))
        self.view.hide()

    def get_tweets(self):
        """
        Get tweets from twitter
        """
        self.view.display("Fetching tweets")

        am = TweepyAPIManager()

        if self.force_random_trend:
            self.trend = am.get_random_trend()

        self.tweets = am.get_tweets(self.trend, 1000)

    def make_image(self, string):
        """
        This method creates an image based on the string provided
        """
        font_size = 17
        spacing = 10
        padding = 10
        kor = Image.open("src/img/kor7070.png")
        img = Image.new("RGB", (480, 480), (236, 210, 244))
        font_poem = ImageFont.truetype("src/fonts/m5x7.ttf", 22)

        poem = add_line_break(string, 60)
        draw = ImageDraw.Draw(img)

        img.paste(kor, (20, 20))

        size = draw.multiline_textsize(poem, font_poem, spacing)

        new_pos = [
            (20, 75 + kor.size[1]),
            (size[0] + 20 + padding, size[1] + 75 + padding + kor.size[1])
        ]

        draw.rectangle(new_pos, fill=(255, 255, 255))

        draw.polygon([
            (85, 85),
            (160, 150),
            (110, 150)
            ], fill=(255, 255, 255)
        )

        draw.multiline_text(
            (new_pos[0][0] + padding, new_pos[0][1] + padding),
            poem,
            font=font_poem,
            fill=(0, 0, 0),
            spacing=spacing
        )
        return img.save("tweet.png")

    def make_poem(self):
        """
        Creates a poem
        """

        current_scheme = self.schemes[randrange(0, len(self.schemes))]
        self.view.display("Whooo.. it'll be {}".format(current_scheme))

        poem = []
        mentions = []
        rhyme_made = False

        while rhyme_made is False:
            poemdict = defaultdict(list)

            for label, number in Counter(current_scheme).items():
                found = False
                while found is False:
                    """
                    Search untill you find a rhyme
                    """
                    temp = []
                    start_sentence = self.tweets.pop(
                        randrange(0, len(self.tweets))
                    )
                    word = find_word_to_rhyme(start_sentence[1])
                    if word is None:
                        continue
                    rhyming_words = find_rhyming_words(
                        word, self.dictionary, self.rhyme_dictionary
                    )

                    temp.append(start_sentence)

                    rhyming_candidates = []
                    for tweet in self.tweets:
                        """
                        Loop through all tweets available
                        """
                        tweet_word = find_word_to_rhyme(tweet[1])
                        if tweet_word in rhyming_words:
                            rhyming_candidates.append(tweet)

                    if len(rhyming_candidates) >= number-1:
                        """
                        If you find enought rhyming sentences you find the
                        tweet congrats we created poetry
                        """
                        for i in range(number-1):
                            temp.append(
                                rhyming_candidates.pop(
                                        randrange(0, len(rhyming_candidates))
                                    )
                                )

                        poemdict[label] = temp
                        found = True
                    else:
                        """
                        We dont have enough inspiration this time
                        good luck next time, art takes time to create
                        """
                        self.view.display("No inspiration! {} sentences left"
                                          .format(len(self.tweets)))
                        if len(self.tweets) == 0:
                            return None

            for label in current_scheme:
                selected = poemdict[label].pop()
                poem.append(selected[1])
                mentions.append(selected[0])

            rhyme_made = True

        return ('\n'.join(poem), mentions)
