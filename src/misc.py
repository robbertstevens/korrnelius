import sys
import re
import unicodedata
import base64


def progress(i, total):
    """
    Prints i/total as a precentage
    """
    point = total / 100
    sys.stdout.write("\r {0:.2f}%".format((i / point)))
    sys.stdout.flush()


def tokenize(line):
    """
    Tokenize a string into a list without punctuation
    """

    return "".join([" " if not c.isalpha()
                    else c.lower() for c in line]).split()


def is_within(point, image):
    """
    Takes a graphics.Point and a graphics.Image and returns a bool value
    indicating whether the point lies within the image.
    """

    anchor = image.getAnchor()
    width = image.getWidth()
    height = image.getHeight()
    return anchor.x - width / 2 <= point.x <= anchor.x + width / 2 \
        and anchor.y - height / 2 <= point.y <= anchor.y + height / 2


def add_line_break(s, length):
    """
    Adds linebreaks in s at when line is > length
    returns the new string
    """

    ns = ""
    counter = 0
    for w in s.split(" "):

        counter += len(w)+1
        if "\n" in w:
            counter = 0
        if counter > length:
            ns += "\n{} ".format(w)
            counter = 0
            counter += len(w)+1
        else:
            counter += 1
            ns += "{} ".format(w)

    return ns.strip()


def split_in_sentences(sentence):
    """
    Splits a string on .?! and returns the result as a list
    """

    splitted = re.split(r"[\.\?\!\;\:]", sentence)
    splitted = [s.strip() for s in splitted if s != ""]

    return splitted


def string_to_folder(name):
    return str(base64.b64encode(name.encode('utf-8'))).encode("utf-8")
