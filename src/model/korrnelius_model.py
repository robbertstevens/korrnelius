from random import randrange
from twitter import *


class Tweet:

    def __init__(self, content):
        self.date = datetime.datetime.now()
        self.content = content

    def post(self):
        am = TweepyAPIManager()
        am.post_tweet(self.content)


class Word:

    def __init__(self, word, accent, ps, ss):
        self.word = word
        self.accent = accent
        self.phonetic = ps

        # [VVC][CV[C]V][CV][CVV][CVC]
        nss = []
        syllable_list = ss[1:-1].split("][")
        for syl in syllable_list:
            if "[" not in syl:
                nss.append(syl)
                continue

            for s in syl.split("["):
                nss.append(s.replace("]", ""))

        self.syllable = nss
