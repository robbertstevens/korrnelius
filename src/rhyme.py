import csv
from misc import progress, tokenize
# import nltk
from collections import defaultdict
from model.korrnelius_model import Word


def find_word_to_rhyme(sentence):
    """
    Returns a list with a dict with the word the next sentence should rhyme on
    and the complete sentence
    """
    tokenized = tokenize(sentence)
    for word in tokenized[::-1]:
        if len(word) >= 2:
            return word
    return None


def find_rhyming_words(current, word_dict, rhyme_dict):
    """
    Returns a list of words rhyming on the current word
    """

    rhyming_words = []
    current = current.lower().strip()

    for word in word_dict[current]:
        current_accent = find_accent(word)
        if rhyme_dict[current_accent] not in rhyming_words:
            rhyming_words += rhyme_dict[current_accent]

    return list(set(rhyming_words)-set([current]))


def word_dictionary():
    """
    Reads the dpw.cd file and returns a dictionary with as key a word and as
    value a list contains Word objects
    """

    file_h = open("src/dpw.cd")

    words = defaultdict(list)
    for line in file_h:
        if len(line) == 0:
            return
        line = line.strip()
        l = line.split("\\")
        accent = l[3].split("-")
        syllable = l[5]
        phonetic = l[4]
        word = l[1]
        if len(phonetic) > 0 and len(accent) > 0:
            words[word].append(
                Word(
                    word, accent,
                    phonetic, syllable))

    return words


def rhyme_dictionary(word_dictionary):
    """
    Return a defaultdict with as key the part of a word after the accent
    without any starting vowels. The value contains a list of words that
    rhyme.
    """

    rhyme_dict = defaultdict(list)
    for key, wordlist in word_dictionary.items():
        for word in wordlist:
            rhyme_dict[find_accent(word)].append(key)

    return rhyme_dict


def word_rhymes(word1, word2):
    """Validates if the two given lines are rhyming and returns a Bool"""

    if word1.word == word2.word:
        return False

    w1 = find_accent(word1)
    w2 = find_accent(word2)

    if w1 is None and w2 is None:
        return False
    if w1 == w2:
        return True
    return False


def find_accent(word):
    """
    Returns the part of the word after the accent starting with the first
    vocal
    """
    w = []
    for syl in word.accent[::-1]:
        w.append(syl)
        if '\'' in syl:
            w = w[::-1]
            try:
                syl = word.syllable[-(len(w))]
                w[0] = w[0].replace('\'', '')

                for cv in syl:
                    if cv == "C":
                        w[0] = w[0][1:]
                    else:
                        break

                return ''.join(w)
            except:
                return None
