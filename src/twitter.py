import sys
import csv
import tweepy
import json
import config
import hashlib
from random import randrange
from misc import *


class TweepyAPIManager:

    def __init__(self):
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.access_token, config.access_token_secret)
        self.api = tweepy.API(auth)

    def post_tweet(self, message):
        """
        Tweets the given message on twitter
        """
        self.api.update_status(status=message)

    def post_tweet_with_media(self, message, file):
        """
        Tweets the message and file on twitter
        """
        self.api.update_with_media(
            media=file, filename="tweet.png", status=message)

    def cleanup_data_file(self, data):
        """
        Removes sentences from data when there are over 1500 sentences in the
        file
        """
        if len(data) > 1500:
            print("Removing some old tweets")
            data = data[-1500:]
            with open('src/tweets/tweets.csv', 'w', encoding="utf-8") as outfile:
                csvwriter = csv.writer(
                        outfile, delimiter='\t',
                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for tweet in data:
                    csvwriter.writerow(tweet)

    def get_trend_data_list(self, trend):
        """
        Returns a list of currently stored sentences
        """
        data = []
        try:
            with open('src/tweets/tweets.csv', 'r', encoding="utf-8") as file:
                csvreader = csv.reader(file, delimiter='\t', quotechar='|')
                for row in csvreader:
                    data.append(row)
            self.cleanup_data_file(data)
        except:
            print("No file with tweets yet")
        return data

    def tweets_to_file(self, query, max=1000):
        try:
            data = self.get_trend_data_list(query)
            if len(data) > 0:
                last_id = data[-1][2]
            else:
                last_id = None

            tweets = []
            for status in tweepy.Cursor(
                    self.api.search, since_id=last_id,
                    q=query, lang="nl").items(max):
                if "http" not in status.text:
                    if status.text[0:2] != "RT":
                        for sentence in split_in_sentences(status.text):
                            if sentence != "":
                                tweets.append((
                                        status.user.screen_name,
                                        sentence.strip(), status.id,
                                        status.text))
            if len(tweets) != 0:
                with open('src/tweets/tweets.csv', 'a', encoding="utf-8") as outfile:
                    csvwriter = csv.writer(
                            outfile, delimiter='\t',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for tweet in tweets:
                        csvwriter.writerow(tweet)

        except:
            print("Fetching tweets failed.. fallback on older file")

        data = self.get_trend_data_list(query)
        return data

    def get_tweets(self, query, max=1000):
        """
        Returns a maximum of 1000 tweets matching the query
        """
        return self.tweets_to_file(query, max)

    def get_trends(self):
        """
        Returns the current trends on twitter
        """
        trends = []

        json = self.api.trends_place(23424909)
        for trend in json[0]["trends"]:
            if trend["tweet_volume"] is not None:
                trends.append(trend["name"])

        return trends

    def get_random_trend(self):
        """
        Returns a random trending topic
        """
        trends = self.get_trends()
        return trends[randrange(0, len(trends)-1)]
