import re
from graphics import *
from misc import add_line_break, is_within
from datetime import datetime, timedelta


class View:
    """
    View class other views will inherent from this one
    """
    def show(self):
        """
        Initial show of the view
        """
        pass

    def display(self, model):
        """
        Recal this method to renew the view
        """
        pass

    def get_action(self):
        """
        Get the action that needed to be executed
        """
        pass

    def hide(self):
        """
        Hide the view
        """
        pass


class AutoConsoleView(View):

    def __init__(self, rate):
        self.rate = rate
        self.last_tweet = datetime.now()
        self.posted_last_tweet = False

    def show(self):
        print("Korrnelius!")
        pass

    def display(self, poem):
        pass

    def get_action(self):

        while True:
            if not self.posted_last_tweet:
                """
                Tweet the poem, send the "tweet"-command to the controller
                """
                self.posted_last_tweet = True
                return "tweet"

            if (datetime.now() - self.last_tweet).total_seconds() > self.rate:
                """
                Make a new poem, send "new"-command to the controller
                """
                self.last_tweet = datetime.now()
                self.posted_last_tweet = False
                return "new"

    def hide(self):
        pass


class ConsoleView(View):

    def show(self):
        pass

    def display(self, poem):
        # for model in models:
        print(poem)

    def get_action(self):
        while True:
            print("1) Make a poem")
            print("2) Tweet the poem")
            print("3) Quit")
            command = input('> ')
            return command

    def hide(self):
        pass


class GUIView(View):

    def show(self):
        self.win = GraphWin("Korrnelius", 500, 500)
        self.win.setBackground("#ecd2f4")
        rect = Rectangle(Point(-10, -10), Point(510, 510))
        rect.setFill("#ecd2f4")
        rect.setOutline("#ecd2f4")
        rect.draw(self.win)
        self.font_size = 11
        self.chars_line = 50

        # Buttons
        self.tweet = Image(Point(0, 0), "src/img/tweet.png")
        self.tweet.move(20 + (self.tweet.getWidth() / 2), 400)

        self.new = Image(
            Point((self.win.getWidth() / 2), 400),
            "src/img/new.png"
        )

        self.exit = Image(Point(0, 0), "src/img/exit.png")
        self.exit.move(
            self.win.getWidth() - (self.exit.getWidth() / 2) - 20,
            400
        )

        self.tweet.draw(self.win)
        self.new.draw(self.win)
        self.exit.draw(self.win)

    def display(self, model):
        kor = Image(Point(0, 0), "src/img/kor7070.png")
        kor.move(kor.getWidth() / 2 + 20, kor.getHeight() / 2 + 20)
        kor.draw(self.win)

        ns = add_line_break(model, self.chars_line)

        lines = ns.count("\n") + 1

        p = Point(
            250,
            kor.getAnchor().getY() + (lines * (self.font_size * 1.6) / 2) + 60
        )

        "Rectangle speech bubble"
        p1 = Point(
            p.getX() - 240, (p.getY() - (lines * (self.font_size * 1.6) / 2))
        )

        p2 = Point(
            p.getX() + 240, (p.getY() + (lines * (self.font_size * 1.6) / 2))
        )
        r = Rectangle(p1, p2)
        r.setFill("white")
        r.setOutline("white")
        r.draw(self.win)

        "Triangle Speech bubble"
        tri = Polygon([
            Point(75, 75),
            Point(155, (p.getY() - (lines * (self.font_size * 1.6) / 2))),
            Point(110, (p.getY() - (lines * (self.font_size * 1.6) / 2)))
        ])
        tri.setFill("white")
        tri.setOutline("white")
        tri.draw(self.win)
        t = Text(p, ns)
        t.setFace("courier")
        t.setSize(self.font_size)
        t.draw(self.win)

    def get_action(self):
        while True:
            try:
                click = self.win.getMouse()
            except GraphicsError:
                return "quit"

            if is_within(click, self.tweet):
                return "tweet"

            if is_within(click, self.new):
                return "new"

            if is_within(click, self.exit):
                return "quit"

    def hide(self):
        pass
